const router = require('express').Router();
const Db = require('../db');
const Player = require('../models/player');
const sqlite3 = require('sqlite3');
const methodOverride = require('method-override')

var bodyParser = require("body-parser");
router.use(methodOverride('_method'))
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({
    extended: true
}))

let db = new sqlite3.Database('./database.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
});

//player = new Player('toto');

//db = new Db();

let nbPlayer = 0;


// GET /players
router.get("/", (req, res) => {
    console.log('page players')
    let players = [];

    const sql = "SELECT * FROM Player ORDER BY id";
    db.all(sql, [], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        rows.forEach((row) => {
            players.push(row);
        });
        res.format({
            json: () => {
                res.send(JSON.stringify(players), null, 3)
            },
            html: () => {
                res.render("players", {
                    model: rows
                });
            }
        })
        //console.log(JSON.stringify(players))
    });
});


// POST /players
router.post("/", (req, res) => {
    const sql = "INSERT INTO Player (name, email, gameWin, gameLost, createdAt) VALUES (?, ?, ?, ?, ?)";
    const player = [req.body.name, req.body.email, req.body.gameWin, req.body.gameLost, req.body.createdAt];

    db.run(sql, player, err => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.status(201)
                res.send(JSON.stringify(player), null, 3)
            },
            html: () => {
                res.redirect("/players");
            }
        })
        console.log(player)
    });
});


// GET /players/new
router.get("/new", (req, res) => {
    console.log('page create');
    res.format({
        json: () => {
            res.send(JSON.stringify('erreur 406 NOT_API_AVAILABLE'), null, 3)
        },
        html: () => {
            res.render("create", {
                model: {}
            });
        }
    })
});




// GET /:id
router.get("/:id", (req, res) => {
    console.log('page player id');
    const id = req.params.id;
    const sql = "SELECT * FROM Player WHERE id = ?";
    db.get(sql, id, (err, row) => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.send(JSON.stringify(row.name), null, 3)
            },
            html: () => {
                res.render("player_id", {
                    model: row
                });
            }
        })
        console.log(row.name)
    });
});

// GET /edit/
router.get("/:id/edit", (req, res) => {
    const id = req.params.id;
    const sql = "SELECT * FROM Player WHERE id = ?";
    db.get(sql, id, (err, row) => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.send(JSON.stringify('erreur 406 NOT_API_AVAILABLE'), null, 3)
            },
            html: () => {
                res.render("edit", {
                    model: row
                });
            }
        })
    });
});

// POST /edit/
router.patch("/:id", (req, res) => {
    const id = req.params.id;
    const player = [req.body.name, req.body.email, req.body.gameWin, req.body.gameLost, req.body.createdAt, id];
    const sql = "UPDATE Player SET name = ?, email = ?, gameWin = ?, gameLost = ?, createdAt = ? WHERE (id = ?)";
    db.run(sql, player, err => {
        if (err) {
            res.status(400).json({
                "error": res.message
            })
            return;
        }
        res.format({
            json: () => {
                res.status(200)
                res.send(JSON.stringify(player), null, 3)
            },
            html: () => {
                res.redirect("/players");
            }
        })
    });
});

// GET /delete/
router.get("/:id/delete", (req, res) => {
    console.log('get /delete')
    const id = req.params.id;
    const sql = "SELECT * FROM Player WHERE id = ?";
    db.get(sql, id, (err, row) => {
        if (err) {
            return console.error(err.message);
        }
        res.render("delete", {
            model: row
        });
    });
});

// POST /delete/
router.delete("/:id", (req, res) => {
    const id = req.params.id;
    const sql = "DELETE FROM Player WHERE id = ?";
    db.run(sql, id, err => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.status(204).send()
                //res.send(JSON.stringify(player), null, 3)
            },
            html: () => {
                res.redirect("/players");
            }
        })
    });
});

module.exports = router;