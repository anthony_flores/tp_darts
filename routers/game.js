const sqlite3 = require('sqlite3');
const router = require('express').Router();
const Db = require('../db');
const Player = require('../models/game');


let db = new sqlite3.Database('./database.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
});


// GET /
router.get("/", (req, res) => {
    console.log('page games')
    let games = [];

    const sql = "SELECT * FROM Game ORDER BY id";
    db.all(sql, [], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        rows.forEach((row) => {
            games.push(row);
        });
        res.format({
            json: () => {
                res.send(JSON.stringify(games), null, 3)
            },
            html: () => {
                res.render("games", {
                    model: rows
                });
            }
        })
    });
});


// POST /create
router.post("/", (req, res) => {
    console.log('create id : ' + req.body.id)
    const sql = "INSERT INTO Game (mode, name, currentPlayerId, status, createdAt) VALUES (?, ?, ?, ?, ?)";
    const game = [req.body.mode, req.body.name, req.body.currentPlayerId, req.body.status, req.body.createdAt];
    db.run(sql, game, err => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.status(201)
                res.send(JSON.stringify(game), null, 3)
            },
            html: () => {
                res.redirect("/games");
            }
        })
        console.log(game)
    });
});


// GET /create
router.get("/new", (req, res) => {
    console.log('page create game');
    res.format({
        json: () => {
            res.send(JSON.stringify('erreur 406 NOT_API_AVAILABLE'), null, 3)
        },
        html: () => {
            res.render("create_game", {
                model: {}
            });
        }
    })
});

// GET /:id
router.get("/:id", (req, res) => {
    console.log('get game id')
    const id = req.params.id;
    const sql = "SELECT * FROM Game WHERE id = ?";
    db.get(sql, id, (err, row) => {
        if (err) {
            return console.error(err.message);
        }
        res.render("games_id", {
            model: row
        });
    });
});

// GET /edit/
router.get("/:id/edit", (req, res) => {
    console.log('page edit' + req.params.id)
    const id = req.params.id;
    const sql = "SELECT * FROM Game WHERE id = ?";
    db.get(sql, id, (err, row) => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.send(JSON.stringify('erreur 406 NOT_API_AVAILABLE'), null, 3)
            },
            html: () => {
                res.render("edit_game", {
                    model: row
                });
            }
        })
    });
});

// POST /edit/
router.patch("/:id", (req, res) => {
    console.log('edit ' + req.body.name)
    const id = req.params.id;
    const game = [req.body.mode, req.body.name, req.body.currentPlayerId, req.body.status, req.body.createdAt, id];
    const sql = "UPDATE Game SET mode = ?, name = ?, currentPlayerId = ?, status = ?, createdAt = ? WHERE (id = ?)";
    db.run(sql, game, err => {
        if (err) {
            res.status(400).json({
                "error": res.message
            })
            return;
        }
        res.format({
            json: () => {
                res.status(200)
                res.send(JSON.stringify(game), null, 3)
            },
            html: () => {
                res.redirect("/Games");
            }
        })

    });
});

// GET /delete/
router.get("/:id/delete", (req, res) => {
    console.log('get /delete')
    const id = req.params.id;
    const sql = "SELECT * FROM Game WHERE id = ?";
    db.get(sql, id, (err, row) => {
        if (err) {
            return console.error(err.message);
        }
        res.render("delete_game", {
            model: row
        });
    });
});

// POST /delete/
router.delete("/:id", (req, res) => {
    console.log('delete id : req.params.id')
    const id = req.params.id;
    const sql = "DELETE FROM Game WHERE id = ?";
    db.run(sql, id, err => {
        if (err) {
            return console.error(err.message);
        }
        res.format({
            json: () => {
                res.status(204).send()
            },
            html: () => {
                res.redirect("/Games");
            }
        })
    });
});



router.post("/:id/players", async (req, res) => {
    const sql = "INSERT INTO GamePlayer (GameID, PlayerID) VALUES (?, ?)";

    let playerids = req.body.playersid

    for (let pid of playerids) {

        await db.run(sql, [req.params.id, pid], err => {
            if (err) {
                return console.error(err.message);
            }
        })
    }
    res.format({
        json: () => {
            res.status(204).send()
        },
        html: () => {
            res.redirect(301, "/games/:id/players");
        }
    })
});

router.get("/:id/players", (req, res) => {
    let players = [];
    let playersid = "";
    const sql = `SELECT Playerid FROM GamePlayer WHERE Gameid = ?`;

    db.all(sql, [req.params.id], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        rows.forEach((row) => {
            players.push(row);
            playersid += row.PlayerId + ", ";
        });
        playersid = playersid.slice(0, -2);
        //playersid = "1, 75"
        console.log(playersid)

        const sql2 = `SELECT * from Player WHERE id IN (${playersid})`

        db.all(sql2, [], (err, rows2) => {
            if (err) {
                return console.error(err.message);
            }
            //console.log(rows)


            res.format({
                json: () => {
                    res.status(200)
                    res.send(JSON.stringify(rows2), null, 3)
                },
                html: () => {
                    res.render("players", {
                        model: rows2
                    });
                }
            })
        })
    });
});

router.delete("/:id/players", async (req, res) => {

    let playerids = req.query.id

    console.log(playerids)
    let string = "("

    if (playerids % 1 === 0) {
        string = `${string}${playerids})`
    } else {
        for (pid of playerids) {
            string += `${pid}, `
        }
        string = string.slice(0, -2)
        string += ")"
    }

    const sql = `DELETE FROM GamePlayer WHERE GameId = ${req.params.id} AND PlayerId IN ${string}`;

    db.run(sql, [], err => {
        if (err) {
            return console.error(err.message);
        }

        res.format({
            json: () => {
                res.status(204).send()
            },
            html: () => {
                res.redirect(301, "/games/:id/players");
            }
        })
    });
});


module.exports = router;