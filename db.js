const sqlite3 = require('sqlite3');
const Player = require('./models/player');

let db = new sqlite3.Database('./database.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connexion réussie à la base de données 'database.db'");
});

const sql_create = `CREATE TABLE IF NOT EXISTS Game (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	"mode"	varchar(30),
	"name"	varchar(255),
	"currentPlayerId"	integer,
	"status"	varchar(30),
	"createdAt"	datetime
  );`;
db.run(sql_create, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Game'");
});

const sql_create2 = `CREATE TABLE IF NOT EXISTS Player (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
    "name"	varchar(255),
    "email"	varchar(255),
    "gameWin"	integer DEFAULT 0,
    "gameLost"	integer DEFAULT 0,
    "createdAt"	datetime
  );`;
db.run(sql_create2, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Player'");

  const sql_create3 = `CREATE TABLE IF NOT EXISTS "GamePlayer" (
    "id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
    "GameId"	INTEGER,
    "PlayerId"	INTEGER
  );`;
db.run(sql_create3, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'GamePlayer'");
});


  /*
  // Alimentation de la table
  const sql_insert2 = `INSERT INTO Player (id, name, email, gameWin, gameLost, createdAt) VALUES
  (1, 'Anthony', 'a@a.com', 0, 0, 2020-02-02),
  (2, 'Quentin', 'q@q.com', 0, 0, 2020-02-02),
  (3, 'Corentin', 'c@c.com', 0, 0, 2020-02-02);`;
  db.run(sql_insert2, err => {
    if (err) {
      return console.error(err.message);
    }
    console.log("Alimentation réussie de la table 'Player'");
  });
  */
});

const toEdit = {
  gameWin: "gameWin",
  gameLost: "gameLost"
};




module.exports = class Database {

  getAllPlayers() {
    let players = [];
    return new Promise((resolve, reject) => {
      db.all('select * from Player', [], (err, rows) => {
        if (err)
          throw err;
        rows.forEach((row) => {
          players.push(row);
        });
        resolve(players);
      })
    })
  }

}