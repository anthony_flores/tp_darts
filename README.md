# **TP - Dart Master**

### Enoncé :
Vous avez eu la super idée d'acheter un jeu de fléchettes électroniques pour occuper les pauses
dans votre boîte. Petit soucis, vous oubliez que Antonin (votre collègue un peu nul et bourrin)
aurait de grandes chances de le casser en jouant avec. Quelques jours plus tard, ce qui devait
arrivait arriva, et l'écran du jeu ne fonctionne plus. Vous êtes maintenant obligé de tenir les
scores à la main.
C'est long et pénible, sans compter les erreurs de calculs. Heureusement vous êtes développeur,
vous allez pouvoir faire un progamme qui gère les scores pour 3 modes de jeux.

 - **Le TP se divise en 2 parties :**
	1ère Partie (Moteur): Gérer le calcul du score avec un programme en CLI (Affichage et input
	du score via le terminal), pour 3 modes de jeu différents. À rendre sur branche engine du
	git.
 - **2ème Partie (API):**
	Gérer les inputs via une API REST (Potentiellement utilisable par un 
	ESP/Arduino branché au jeu de flechettes)

#### **Attention !!!**

Lancer le server : **`npm install && npm start`**
