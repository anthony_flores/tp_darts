'use strict';

let Player = require('./models/player');
let Game = require('./models/game');
let Dart = require('./models/dart');

var inquirer = require('inquirer');
var figlet = require('figlet');
const chalk = require('chalk');
const clear = require('clear')


const sqlite3 = require('sqlite3');
const Db = require('./db.js');


let nbplayers = 0;
let points_cricket = 0
let open_close = Boolean

let player = new Player('')
let game = new Game('')
let dart = new Dart('')

var playerarray = []
var gamearray = []
var dartarray = []
/*
var arraysector = [
  [15,16,17,18,19,20,25],
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0]
]
*/


var questions = [{
    type: 'list',
    name: 'gamemode',
    message: "Quel est le mode de jeu ?",
    choices: ['le tour du Monde', 'le 301', 'le Cricket']
  },
  {
    type: 'input',
    name: 'namegame',
    message: 'Quel est le nom du jeu ?',
  },
  {
    type: 'input',
    name: 'nbplayer',
    message: 'Quel est le nombre de joueur ?',
    default: 1,
    validate: function (value) {
      var valid = !isNaN(parseFloat(value)) && value != 0;
      nbplayers = value;
      return valid || 'Please enter a number > 0';
    },
    filter: Number
  }
];

var names = [{
  type: 'input',
  name: 'nameplayer',
  message: 'Quel est le nom du joueur ? ',
}];

var points = [{
    type: 'input',
    name: 'pointplayer',
    message: 'Score',
    default: 1,
    validate: function (value) {
      var valid = !isNaN(parseFloat(value));

      if (valid) {
        if (value == 25 || value <= 20) {
          return true
        } else {
          return 'Please enter a number between 0 and 20 or 25';
        }
      }

    },
    filter: Number
  },
  {
    type: "list",
    name: "sectorplayer",
    message: "Quel est le secteur ?",
    choices: ['simple', 'double', 'triple'],
  }
];

var sector_cricket_choices = [{
  type: 'list',
  name: 'sector_choices',
  message: "Quel secteur voulez-vous toucher ?",
  choices: [15, 16, 17, 18, 19, 20, 25]
}];

var sector_cricket = [

  {
    type: 'list',
    name: 'sector',
    message: "Quel est le secteur touché ?",
    choices: [15, 16, 17, 18, 19, 20, 25]
  },
  {
    type: "list",
    name: "multi",
    message: "Quel est le multiplicateur ?",
    choices: ['simple', 'double', 'triple'],
  }
];

var emailQ = {
  message: "What's your email?",
  type: "input",
  name: "email",
  validate: emailIsValid
};

/*
 * Fonction pour calculer les points du 301 (val1 > point; val2 > multiplicateur) 
 */
function calculpoint(val1, val2) {
  switch (val2) {
    case 'simple':
      var value = val1 * 1
      return value
    case 'double':
      var value = val1 * 2
      return value
    case 'triple':
      var value = val1 * 3
      return value
    default:
      break;
  }
}

/*
 * Fonction pour calculer les points du Cricket (val1 > point; val2 > multiplicateur)
 */
function calculpointCricket(val2) {
  switch (val2) {
    case 'simple':
      var value = 1;
      return value;
    case 'double':
      var value = 2;
      return value;
    case 'triple':
      var value = 3;
      return value;
    default:
      break;
  }
}

/*
 * Fonction pour calculer la date actuelle 
 */
function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var month = date.getMonth();
  var year = date.getFullYear();

  //return day + ' ' + monthNames[monthIndex] + ' ' + year;
  return year + '-' + month + '-' + day;
}

/*
 * Fonction pour créer un UUID unique et aléatoire
 */
function create_UUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

/*
 * Fonction pour vérifier la conformité du format de l'email 
 */
function emailIsValid(email) {
  if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
    return true
  } else {
    return "l'email n'est pas valide"
  }
}


const init = async () => {
  clear()
  console.log(
    chalk.white(
      figlet.textSync('Dart Game ! !', {
        horizontalLayout: "full",
      })
    )
  )
}

let db = new sqlite3.Database('./database.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
});


async function main() {

  let date = formatDate(new Date())
  console.table(date);
  console.table(' ');

  const answers_q = await inquirer.prompt(questions)
  console.log(answers_q)
  //console.log(nbplayers)

  game = new Game(create_UUID(), answers_q.gamemode, answers_q.namegame, answers_q.nbplayer, date)
  gamearray.push(game)
  //let db = new Db();

  const sqlgame0 = "INSERT INTO Game (mode, name, currentPlayerId, status, createdAt) VALUES (?, ?, ?, ?, ?)";
  const game0 = [game._modegame, game._namegame, 0, game._statusgame, game._createdAt];
  db.run(sqlgame0, game0, err => {
    if (err) {
      return console.error(err.message);
    }
  });

  /*
   * Création des joueurs avec leurs informations
   */
  for (var i = 1; i <= nbplayers; nbplayers--) {

    let score_base = game._playerscore
    //console.log('le score de base est ' + score_base)
    const answers_name = await inquirer.prompt(names)
    const answers_email = await inquirer.prompt(emailQ)
    //console.log(answers_name.nameplayer)

    player = new Player(create_UUID(), date, answers_name.nameplayer, score_base, answers_email.email)
    playerarray.push(player)

    dart = new Dart(answers_name.nameplayer, points_cricket)
    dartarray.push(dart)

    var sqlplayer0 = "INSERT INTO Player (name, email, gamewin, gamelost, createdat) VALUES (?, ?, ?, ?, ?)";
    var player0 = [player._playername, player._playeremail, player._gamewin, player._gamelost, player._createdAt]
    db.run(sqlplayer0, player0, err => {
      if (err) {
        return console.error(err.message);
      }
    })
  }

  console.table(playerarray)
  console.table(dartarray)



  switch (game._modegame) {
    /////////////////////////////////////////:LE TOUR DU MONDE://////////////////////////////////////:

    case 'le tour du Monde':
      console.log(chalk.black.bgWhite.bold('"le tour du Monde" Commence !!'));

      var numplayer = Math.floor(Math.random() * playerarray.length);
      var nameplayer = playerarray[numplayer]._playername;
      console.log(nameplayer + " Commance à tirer !!")
      console.log(chalk.black.bgCyanBright.bold(nameplayer + " Commance à tirer !!"));

      game._statusgame = 'started'

      while (game._statusgame == 'started') {
        let nbplayers1 = game._nbplayer
        //console.log(nbplayers1)

        for (var i = 1; i <= nbplayers1; nbplayers1--) {

          console.log(chalk.black.bgWhite.bold((playerarray[numplayer]._playername) + ' joue !'));
          //console.log((playerarray[numplayer]._playername) + ' joue !')
          game._currentPlayerId = playerarray[numplayer]._playername

          //console.table(game)

          for (let j = 0; j < 3; j++) {
            const answers_point = await inquirer.prompt(points)

            if (answers_point.pointplayer == (playerarray[numplayer]._playerscore + 1)) {
              console.log(chalk.white.bgRed.bold('+1 !!!!'))
              playerarray[numplayer]._playerscore = playerarray[numplayer]._playerscore + 1
            }

            if (playerarray[numplayer]._playerscore == 20) {
              game._statusgame = 'ended'
              console.log(chalk.white.bgGreen('La partie est gagné par : ') + chalk.white.bgGreen.bold(playerarray[numplayer]._playername) + chalk.white.bgGreen(' '))

              for (var i = 0; i < nbplayers1; i++) {
                playerarray[i]._gamelost++
              }
              playerarray[numplayer]._gamewin++
              playerarray[numplayer]._gamelost--

              console.table(playerarray)

              break;
            }
          }
          numplayer++
          if (numplayer == game._nbplayer) {
            numplayer = 0
          }
        }
        const sqlplayer1 = [player._playername, player._playeremail, player._gamewin, player._gamelost, player._createdAt, player._playername];
        const player1 = "UPDATE Player SET name = ?, email = ?, gameWin = ?, gameLost = ?, createdAt = ? WHERE (name = ?)";
        db.run(player1, sqlplayer1, err => {
          if (err) {
            res.status(400).json({
              "error": res.message
            })
            return;
          }
        });

        const sqlgame1 = [game._modegame, game._namegame, 0, game._statusgame, game._createdAt, game._namegame];
        const game1 = "UPDATE Game SET mode = ?, name = ?, currentPlayerId = ?, status = ?, createdAt = ? WHERE (name = ?)";
        db.run(game1, sqlgame1, err => {
          if (err) {
            return console.error(err.message);
          }
        });
      }

      break;

      /////////////////////////////////////////:LE 301://////////////////////////////////////:
    case 'le 301':
      console.log(chalk.black.bgWhite.bold('"le 301 " Commence !!'));

      var numplayer = Math.floor(Math.random() * playerarray.length);
      var nameplayer = playerarray[numplayer]._playername;
      console.log(chalk.black.bgCyanBright.bold(nameplayer + " Commance à tirer !!"))

      console.table(playerarray)

      let point = 0
      game._statusgame = 'started'

      while (game._statusgame == 'started') {
        let nbplayers1 = game._nbplayer
        //console.log(nbplayers1)

        for (var i = 1; i <= nbplayers1; nbplayers1--) {
          console.log(chalk.black.bgWhite.bold((playerarray[numplayer]._playername) + ' joue !'))
          game._currentPlayerId = playerarray[numplayer]._playername

          //console.table(game)

          for (let j = 0; j < 3; j++) {
            const answers_point = await inquirer.prompt(points)

            point = calculpoint(answers_point.pointplayer, answers_point.sectorplayer)
            console.log(point + " points")
            if (point > playerarray[numplayer]._playerscore) {
              console.log("Tu ne peux pas faire plus que ton score " + playerarray[numplayer]._playerscore)
              playerarray[numplayer]._playerscore = playerarray[numplayer]._playerscore + point
            }

            playerarray[numplayer]._playerscore = playerarray[numplayer]._playerscore - point

            if (playerarray[numplayer]._playerscore == 1) {
              playerarray[numplayer]._playerscore = playerarray[numplayer]._playerscore + point
              console.log("On doit finir avec un double donc Impossible d'avoir 1")
            }

            if (playerarray[numplayer]._playerscore == 0 && answers_point.sectorplayer != 'double') {
              playerarray[numplayer]._playerscore = playerarray[numplayer]._playerscore + point
              console.log("On doit finir avec un double")
            }

            console.log(chalk.white.bgRed.bold('Il reste ' + playerarray[numplayer]._playerscore + ' à marquer'))
            playerarray.push(player)

            if (playerarray[numplayer]._playerscore == 0) {
              game._statusgame = 'ended'
              console.log(chalk.white.bgGreen('La partie est gagné par : ' + playerarray[numplayer]._playername))

              for (var i = 0; i < nbplayers1; i++) {
                playerarray[i]._gamelost++
              }
              playerarray[numplayer]._gamewin++
              playerarray[numplayer]._gamelost--



              //console.table(playerarray)

              break;
            }

          }
          numplayer++
          if (numplayer == game._nbplayer) {
            numplayer = 0
          }
        }

        const sqlplayer2 = [player._playername, player._playeremail, player._gamewin, player._gamelost, player._createdAt, player._playername];
        const player2 = "UPDATE Player SET name = ?, email = ?, gameWin = ?, gameLost = ?, createdAt = ? WHERE (name = ?)";
        db.run(player2, sqlplayer2, err => {
          if (err) {
            res.status(400).json({
              "error": res.message
            })
            return;
          }
        });

        const sqlgame2 = [game._modegame, game._namegame, 0, game._statusgame, game._createdAt, game._namegame];
        const game2 = "UPDATE Game SET mode = ?, name = ?, currentPlayerId = ?, status = ?, createdAt = ? WHERE (name = ?)";
        db.run(game2, sqlgame2, err => {
          if (err) {
            return console.error(err.message);
          }
        });
      }


      break;

      /////////////////////////////////////////:LE CRICKET://////////////////////////////////////:
    case 'le Cricket':
      console.log(chalk.black.bgWhite.bold('"le Cricket " Commence !!'));
      console.table(gamearray)

      var numplayer = Math.floor(Math.random() * playerarray.length);
      console.log('n° ' + numplayer + " sur " + playerarray.length + " " + answers_q.nbplayer)
      var nameplayer = playerarray[numplayer]._playername;
      console.log(chalk.black.bgCyanBright.bold(nameplayer + " Commance à tirer !!"))

      console.table(playerarray)

      let point_cricket = 0
      game._statusgame = 'started'

      while (game._statusgame == 'started') {
        let nbplayers1 = game._nbplayer
        //console.log(nbplayers1)

        //for (var i = 0; i <= nbplayers1; nbplayers1--) {
        for (var i = 0; i < nbplayers1; i++) {
          console.log(chalk.black.bgWhite.bold((playerarray[numplayer]._playername) + ' joue !'))

          console.table(dartarray[numplayer])
          console.table(dartarray[numplayer]._sectors)

          game._currentPlayerId = playerarray[numplayer]._playername

          const answers_sector_cricket_choices = await inquirer.prompt(sector_cricket_choices)

          for (let j = 0; j < 3; j++) {

            const answers_sector_cricket = await inquirer.prompt(sector_cricket)
            if (answers_sector_cricket.sector == answers_sector_cricket_choices.sector_choices) {
              console.log('Lancé réussis sur le secteur ' + answers_sector_cricket.sector)
              point_cricket = calculpointCricket(answers_sector_cricket.multi)

              switch (answers_sector_cricket.sector) {
                case 15:
                  if (dartarray[numplayer]._sectors[1][0] < 3) {
                    dartarray[numplayer]._sectors[1][0] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][0] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][0] >= 3) {
                    dartarray[numplayer]._sectors[1][0] = 3
                  }
                  break;
                case 16:
                  if (dartarray[numplayer]._sectors[1][1] < 3) {
                    dartarray[numplayer]._sectors[1][1] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][1] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][1] >= 3) {
                    dartarray[numplayer]._sectors[1][1] = 3
                  }
                  break;
                case 17:
                  if (dartarray[numplayer]._sectors[1][2] < 3) {
                    dartarray[numplayer]._sectors[1][2] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][2] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][2] >= 3) {
                    dartarray[numplayer]._sectors[1][3] = 3
                  }
                  break;
                case 18:
                  if (dartarray[numplayer]._sectors[1][3] < 3) {
                    dartarray[numplayer]._sectors[1][3] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][3] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][3] >= 3) {
                    dartarray[numplayer]._sectors[1][3] = 3
                  }
                  break;
                case 19:
                  if (dartarray[numplayer]._sectors[1][4] < 3) {
                    dartarray[numplayer]._sectors[1][4] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][4] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][4] >= 3) {
                    dartarray[numplayer]._sectors[1][4] = 3
                  }
                  break;
                case 20:
                  if (dartarray[numplayer]._sectors[1][5] < 3) {
                    dartarray[numplayer]._sectors[1][5] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][5] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][5] >= 3) {
                    dartarray[numplayer]._sectors[1][5] = 3
                  }
                  break;
                case 25:
                  if (dartarray[numplayer]._sectors[1][6] < 3) {
                    dartarray[numplayer]._sectors[1][6] += point_cricket
                  } else {
                    dartarray[numplayer]._sectors[2][6] += calculpoint(answers_sector_cricket.sector, answers_sector_cricket.multi)
                  }
                  if (dartarray[numplayer]._sectors[1][6] >= 3) {
                    dartarray[numplayer]._sectors[1][6] = 3
                  }
                  break;

              }

            } else {
              console.log('Lancé loupé sur ' + answers_sector_cricket.sector)
            }
            if (j < 2) {
              console.table(dartarray[numplayer]._sectors)
            }
          }

          for (let l = 0; l < 7; l++) {
            if (dartarray[numplayer]._sectors[1][l] == 3) {
              points_cricket += 1
            }
          }

          console.table(dartarray[numplayer])
          console.table(dartarray[numplayer]._sectors)

          if (dartarray[numplayer]._sectorclose == 7) {
            game._statusgame = 'ended'
            console.log(chalk.white.bgGreen('La partie est gagné par : ' + playerarray[numplayer]._playername))
            for (var i = 0; i < nbplayers1; i++) {
              playerarray[i]._gamelost++
            }
            playerarray[numplayer]._gamewin++
            playerarray[numplayer]._gamelost--
            console.table(playerarray)
            break;

          }

          numplayer++
          if (numplayer == game._nbplayer) {
            numplayer = 0
          }
        }
        const sqlplayer3 = [player._playername, player._playeremail, player._gamewin, player._gamelost, player._createdAt, player._playername];
        const player3 = "UPDATE Player SET name = ?, email = ?, gameWin = ?, gameLost = ?, createdAt = ? WHERE (name = ?)";
        db.run(player3, sqlplayer3, err => {
          if (err) {
            res.status(400).json({
              "error": res.message
            })
            return;
          }
        });

        const sqlgame3 = [game._modegame, game._namegame, 0, game._statusgame, game._createdAt, game._namegame];
        const game3 = "UPDATE Game SET mode = ?, name = ?, currentPlayerId = ?, status = ?, createdAt = ? WHERE (name = ?)";
        db.run(game3, sqlgame3, err => {
          if (err) {
            return console.error(err.message);
          }
        });
      }

      break;

    default:
      console.log('default');
  }
}

init()
main()