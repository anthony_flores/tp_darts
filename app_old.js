const Router = require('./routes');
const app = require('express')();
const bodyParser = require('body-parser');
const methodOverride = require('method-override')
const chalk = require('chalk');
const express = require("express");
const path = require("path");


app.use(methodOverride('_method'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(Router)

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");
app.use(express.static(path.join(__dirname, "assets"))); // Permet d'envoyer les fichiers statiques qui sont dans le dossiers assets.
app.use(express.urlencoded({
  extended: false
}));

// GET /about
app.get("/about", (req, res) => {
  console.log(chalk.black.cyan.bold('http://127.0.0.1:8080/about'));
  res.render("about");
});

// GET / => accueil
app.get("/", (req, res) => {
  console.log(chalk.black.cyan.bold('http://127.0.0.1:8080/'));
  res.render("index");
});

// GET /test
app.get("/test", (req, res) => {
  const test = {
    titre: "Test",
  };
  console.log(chalk.black.cyan.bold('http://127.0.0.1:8080/test'));
  res.render("test", {
    model: test
  });
});

// Démarrage du serveur
app.listen(8080);