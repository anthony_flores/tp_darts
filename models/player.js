
module.exports = class Player {
  constructor(playeruuid, datetime, playername, playerscore, email) {
    this._id = playeruuid;
    this._createdAt = datetime;
    this._playername = playername;
    this._playerscore = playerscore;
    this._playeremail = email;
    this._gamewin = 0;
    this._gamelost = 0;
  }


  /**
     * Renvoie le nom du joueur.
     * @returns {string}
     */
    get name() {
        return this._playername;
    }

    get email() {
        return this._playeremail;
    }

    /**
     * Renvoie le score du joueur.
     * @returns {number}
     */
    get score() {
        return this._playerscore;
    }

    /**
     * Renvoie la valeur associée au dernier secteur touché par le joueur.
     * @returns {number}
     */
    get lshot() {
        return this.lastShot;
    }

    /*get cricketArray() {
        return this.cricketArray;
    }*/

    /**
     * Permet de définir le nom du joueur.
     * @param {string} playername 
     */
    setName(playername) {
        this._playername = playername;
    }

    /**
     * Permet de définir le score du joueur.
     * @param {number} playerscore 
     */
    setScore(playerscore) {
        this._playerscore = playerscore;
    }

    /**
     * Permet de définir la valeur associée au dernier secteur touché par le joueur.
     * @param {string} shot 
     */
    setLastShot(shot) {
        this.lastShot = shot;
    }

    set email (playermail) {
        this._playeremail = playermail;
    }
}

//////////// TEST
/*let players1 = new Player('anthony', 154)

console.table(players1.Name)
console.table(players1.Score)
console.table(players1.Allplayer)
*/