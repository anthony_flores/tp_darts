module.exports = class Game {

  constructor(gameuuid ,modegame, namegame, nbplayer, datetime) {
      this._id = gameuuid;
      this._modegame = modegame;
      this._namegame = namegame
      this._nbplayer = nbplayer;
      this._currentPlayerId = '';
      this._statusgame = 'dart';
      this._createdAt = datetime;


       switch (this._modegame) {
        case 'le tour du Monde':
          //console.log('Mode1 ');
          this._playerscore = 0
          break;
        case 'le 301':
          //console.log('Mode 2');
          this._playerscore = 301
          break;
        case 'le Cricket':
          //console.log('Mode 3');
          this._playerscore = 0
          break;
        default:
      }
    }


    /**
     * Renvoie le mode de jeu.
     * @returns {string}
     */
    getMode(){
        return this._modegame;
    }


    /**
     * Permet de définir le mode de jeu.
     * @param {string} modegame 
     */
    setMode(modegame){
        this._modegame = modegame;
    }

    /**
     * Permet de définir le nombre de joueurs.
     * @param {number} _nbPlayers 
     */
    setNbPlayers(nbPlayers){
        this._nbplayer = nbplayer;
    }

}

  //////////// TEST
  /*
  let game = new Game('le tour du monde', 2, 3);

  console.log(game.Modegame)
  console.log(game.Nbplayer)
  console.log(game.Allgame)
  */

  